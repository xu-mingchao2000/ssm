<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>学生列表页</title>
<link href="http://libs.baidu.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
<table class="table table-striped table-bordered">
	<thead>
		<tr><td>ID</td><td>姓名</td><td>性别</td></tr>
	</thead>
	<tbody>
		<c:forEach var="user" items="${data}">
			  <tr>
			  	<td>${user.id}</td>
			  	<td>${user.studentName}</td>
			  	<td>${user.studentSex}</td>
			  </tr>
		</c:forEach>
	</tbody>
</table>
</body>
</html>