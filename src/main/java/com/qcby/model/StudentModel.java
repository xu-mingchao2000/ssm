package com.qcby.model;

import java.util.List;

import com.qcby.entity.Student;

public class StudentModel {
	private List<Student> students;

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	@Override
	public String toString() {
		return "StudentModel [students=" + students + "]";
	}
	

}
