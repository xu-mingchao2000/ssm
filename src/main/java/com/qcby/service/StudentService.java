package com.qcby.service;

import java.util.List;

import com.qcby.entity.Student;
/**
 * 学生相关的服务
 * @author yhtgb
 *
 */
public interface StudentService {
	/**
	   *  添加学生
	 * @param student 要添加的学生
	 * @return 受影响的行数
	 */
	int add(Student student);
	/**
	 * 查询学生
	 * @param id 学生id
	 * @return 查询到的学生
	 */
	Student find(Integer id);
	/**
	 * 查询学生
	 * @param student 学生信息
	 * @return 符合条件的学生
	 */
	List<Student> findListByStudent(Student student);
	
}
