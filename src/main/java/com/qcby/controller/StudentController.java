package com.qcby.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.qcby.entity.Student;
import com.qcby.model.StudentModel;
import com.qcby.service.StudentService;

@Controller
@RequestMapping("student")
public class StudentController {
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("add")
	public String add(Student student) {
		System.out.println(student);
		//返回的是调用添加方法数据库受影响的行数
		int addNum = studentService.add(student);
		System.out.println(addNum);
		return "add";
	}
	@RequestMapping("addList")
	public String addList(StudentModel model) {
		model.getStudents().forEach(e->{
			System.out.println(e);
		});
		return "add";
	}
	@RequestMapping("findById")
	@ResponseBody
	public Student findByid(@RequestBody Student id) {
		return studentService.find(id.getId());
		
	}
	@RequestMapping("findByStudent")
	public String findByStudent(Student student,Model model) {
		List<Student> data = studentService.findListByStudent(student);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		model.addAttribute("data", data);
		return "list";
		
	}
	@RequestMapping("test")
	@ResponseBody
	public Student test(Student student) {

		return student;

	}
}
